<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sales
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Sales
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(name="quatity", type="integer")
     */
    private $quantity;

    /**
     * @var datetime
     * @ORM\Column(name="dt_created", type="datetime")
     */
    private $dtCreated;

    /**
     * @var decimal
     * @ORM\Column(name="price",type="decimal", precision=7, scale=2)
     */
    private $price=0;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="sales")
     */
    private $product;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
