<?php
/**
 * Created by PhpStorm.
 * User: bruno
 * Date: 05/08/15
 * Time: 20:07
 */

namespace AppBundle\Controller\Admin;


use AppBundle\Entity\Product;
use AppBundle\Form\PostType;
use AppBundle\Form\ProductType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductController
 * @package AppBundle\Controller\Admin
 * @Route("/admin/product")
 * @Security("has_role('ROLE_ADMIN')")
 */
class ProductController extends  Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="admin_product_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $products= $em->getRepository("AppBundle:Product")->findAll();
        return $this->render('admin/product/index.html.twig', array('products' => $products));
    }

    /**
     * @Route("/new",name="admin_produto_new")
     * @Method({"GET","POST"})
     */
    public function addAction(Request $request)
    {
        $product = new Product();
        $form = $this->createForm(new ProductType(),$product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();
            return $this->redirectToRoute('admin_product_edit', array('id' => $product->getId()));
        }

        return $this->render('admin/product/new.html.twig', array(
            'form' => $form->createView(),
            'product'=>$product
        ));
    }

    /**
     * @param Product $product
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/edit{id}",requirements={"id"= "\d+"}, name="admin_product_edit")
     *
     */
    public function editAction(Product $product, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $editForm = $this->createForm(new ProductType(), $product);
        $deleteForm = $this->createDeleteForm($product);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();
            return $this->redirectToRoute('admin_product_edit', array('id' => $product->getId()));
        }

        return $this->render('admin/product/edit.html.twig', array(
            'product'        => $product,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/show/{id}",requirements={"id"="\d+"},name="admin_product_show")
     */
    public function showAction(Product $product)
    {
        $deleteForm = $this->createDeleteForm($product);

        return $this->render('admin/product/show.html.twig', array(
            'product'        => $product,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @param Request $request
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/delete",name="admin_product_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Product $product)
    {
        $form = $this->createDeleteForm($product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->remove($product);
            $em->flush();
        }

        return $this->redirectToRoute('admin_product_index');
    }

    private function createDeleteForm(Product $product)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_product_delete', array('id' => $product->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}